# JOGAR COM DADOS

JCD é um projeto que envolve o tratamento e simulação de dados não reais.

Após o clone ou fork do projeto, certifique que tenha a versão local do **python 3.9**. Instale as dependências do projeto executando a instrução:
```
$ pipenv install
```

Visualize a geração das 6 dezenas acessando o seguinte link:
*https://glcdn.githack.com/rodrigmars/jogarcomdados/-/raw/main/page/index.html*

Para a captação das dezenas via sistema, abra um terminal e execute a seguinte instrução:
```
$ pipenv run app
```
