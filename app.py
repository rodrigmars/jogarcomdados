import asyncio
import sys
from bs4 import BeautifulSoup
from pyppeteer import launch
from datetime import datetime


def get_diff_days(date_start, date_end, format_date):

    def convert(date_text, format_): return \
        datetime.strptime(date_text, format_)

    def calc_delta(d1: datetime, d2: datetime): return (d2 - d1).days

    return calc_delta(convert(date_start, format_date),
                      convert(date_end, format_date))


async def main():

    try:

        url = "https://glcdn.githack.com/rodrigmars/jogarcomdados/-/raw/main/page/index.html"

        browser = await launch()

        page = await browser.newPage()

        await page.goto(url)

        page_content = await page.content()

        soup = BeautifulSoup(page_content, features="html.parser")

        print(soup.find(id="span_pontos").get_text())

        # format_date = "%d/%m/%Y"

        # format_date_time = "%d/%m/%Y %H:%M:%S"

        # total_days = get_diff_days("15/08/2022",
        #                            "23/08/2022",
        #                            format_date)

        # message = f"FALTAM {total_days} DIA(S) PARA O SORTEIO"

        # if total_days <= 0:

        #     message = f"REALIZADO À {abs(total_days)} DIA(S)" \
        #         if abs(total_days) >= 1 else "JÁ REALIZADO"

        #     message = f"SORTEIO {message}"

        # print(message)

        return 0

    except Exception as err:

        print("Erro:", err)

        return 1

if __name__ == '__main__':

    asyncio.get_event_loop().run_until_complete(main())
    # sys.exit(main())
